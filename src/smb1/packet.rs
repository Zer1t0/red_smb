use crate::smb1::Smb1NegReq;
use crate::Result;


#[derive(Clone, Debug, PartialEq)]
pub enum Smb1Msg {
    NegReq(Smb1NegReq)
}

impl Smb1Msg {

    pub fn build(&self) -> Vec<u8> {
        return match self {
            Self::NegReq(m) => m.build()
        }
    }

    pub fn parse(_raw: &[u8]) -> Result<Self> {
        todo!()
    }
}
