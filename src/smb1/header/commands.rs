//! The codes of commands defined in [MS-CIFS] (secion 2.2.2.1)[https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-cifs/32b5d4b7-d90b-483f-ad6a-003fd110f0ec]
//!

pub const SMB_COM_CREATE_DIRECTORY: u8 = 0x00;
pub const SMB_COM_DELETE_DIRECTORY: u8 = 0x01;
pub const SMB_COM_NEGOTIATE: u8 = 0x72;
