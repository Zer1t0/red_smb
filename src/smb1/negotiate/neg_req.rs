use crate::smb1::header::commands::SMB_COM_NEGOTIATE;
use crate::smb1::Smb1Header;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Smb1NegReq {
    pub header: Smb1Header,
    pub body: Smb1NegReqBody,
}

impl Smb1NegReq {
    pub fn new() -> Self {
        return Self {
            header: Smb1Header::new(SMB_COM_NEGOTIATE),
            body: Smb1NegReqBody::default(),
        };
    }

    pub fn build(&self) -> Vec<u8> {
        let mut raw = self.header.build();
        raw.extend(self.body.build());
        return raw;
    }
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Smb1NegReqBody {
    pub dialects: Vec<String>,
}

impl Smb1NegReqBody {
    pub fn build(&self) -> Vec<u8> {
        let mut dialect_bytes = Vec::new();
        for dialect in &self.dialects {
            dialect_bytes.push(0x2);
            dialect_bytes.append(&mut dialect.as_bytes().to_vec());
            dialect_bytes.push(0x0);
        }

        let mut bytes = vec![0];
        bytes.append(&mut (dialect_bytes.len() as u16).to_le_bytes().to_vec());
        bytes.append(&mut dialect_bytes);

        return bytes;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::smb1::negotiate::dialects::SMB_DIA_NT_LM;

    const RAW_NEG_REQ_BODY: &'static [u8] = &[
        0x00, 0x0c, 0x00, 0x02, 0x4e, 0x54, 0x20, 0x4c, 0x4d, 0x20, 0x30, 0x2e,
        0x31, 0x32, 0x00,
    ];

    #[test]
    fn test_build_neg_req() {
        let neg_req = Smb1NegReqBody {
            dialects: vec![SMB_DIA_NT_LM.to_string()],
        };

        assert_eq!(RAW_NEG_REQ_BODY, neg_req.build());
    }
}
