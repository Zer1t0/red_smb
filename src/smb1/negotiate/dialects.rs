
// SMB dialects

/// Indicates that SMB1 (defined in [MS-SMB]) is supported.
pub const SMB_DIA_NT_LM: &'static str = "NT LM 0.12";

/// Indicates that the SMB 2.0.2 version is supported.
pub const SMB_DIA_SMB_2_002: &'static str = "SMB 2.002";

/// Indicates that the SMB 2.1 version or 3.x family version are supported.
pub const SMB_DIA_SMB_2_QUESTION: &'static str = "SMB 2.???";
