use crate::Result;
use nom::bytes::complete::{take};
use crate::smb1::SMB1_ID;
use crate::smb2::SMB2_ID;
use std::convert::TryInto;

#[derive(Debug)]
pub enum SmbVersion {
    V1,
    V2,
    Unknown
}

pub fn check_smb_version(raw: &[u8]) -> Result<SmbVersion> {
    let magic = take(4usize)(raw)?.1;

    return Ok(match magic.try_into().unwrap() {
        SMB1_ID => SmbVersion::V1,
        SMB2_ID => SmbVersion::V2,
        _ => SmbVersion::Unknown
    });
}
