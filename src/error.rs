use crate::smb2::Smb2Msg;
use crate::smb1::Smb1Msg;
use thiserror::Error;
use nom;
use std::io;

pub type Result<T> = std::result::Result<T, Error>;


#[derive(Debug, Error)]
pub enum Error {
    #[error("No enough data was received")]
    NotEnoughData,

    #[error("{0}")]
    GenericParserError(String),

    #[error("{0}")]
    IO(io::Error),

    #[error("Unknown packet version")]
    UnknownVersion,

    #[error("Unknown SMB1 command {0}")]
    Smb1UnknownCommand(u8),

    #[error("Unexpected SMB1 response")]
    Smb1UnexpectedResponse(Smb1Msg),

    #[error("Unknown SMB2 command {0}")]
    Smb2UnknownCommand(u16),

    #[error("Unexpected SMB2 response")]
    Smb2UnexpectedResponse(Smb2Msg),
}

impl From<nom::Err<nom::error::Error<& [u8]>>> for Error {
    fn from(e: nom::Err<nom::error::Error<& [u8]>>) -> Self {
        if let nom::Err::Error(ref e) = e {
            if e.code == nom::error::ErrorKind::Eof {
                return Self::NotEnoughData;
            }
        }
        return Self::GenericParserError(e.to_string());
    }
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        return Self::IO(e);
    }
}
