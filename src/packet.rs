use crate::smb2::Smb2Msg;
use crate::smb1::Smb1Msg;
use crate::utils::{check_smb_version, SmbVersion};
use crate::{Error, Result};
use crate::smb2::Smb2NegResp;


#[derive(Clone, Debug, PartialEq)]
pub enum SmbMsg {
    V1(Smb1Msg),
    V2(Smb2Msg)

}

impl SmbMsg {

    pub fn build(&self) -> Vec<u8> {
        match self {
            Self::V1(p) => p.build(),
            Self::V2(p) => p.build(),
        }
    }

    pub fn parse(raw: &[u8]) -> Result<Self> {
        let smb_version = check_smb_version(&raw)?;

        return Ok(match smb_version {
            SmbVersion::V1 => SmbMsg::V1(Smb1Msg::parse(raw)?),
            SmbVersion::V2 => SmbMsg::V2(Smb2Msg::parse(raw)?),
            _ => return Err(Error::UnknownVersion),
        });
    }
}


#[derive(Clone, Debug, PartialEq)]
pub enum SmbNegResp {
    V2(Smb2NegResp)
}
