
pub const SMB2_DIA_202: u16 = 0x0202;
pub const SMB2_DIA_210: u16 = 0x0210;
pub const SMB2_DIA_2FF: u16 = 0x02ff;
pub const SMB2_DIA_300: u16 = 0x0300;
pub const SMB2_DIA_302: u16 = 0x0302;
pub const SMB2_DIA_311: u16 = 0x0311;
