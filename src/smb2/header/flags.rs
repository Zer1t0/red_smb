//! SMB2 flags

/// Indicates that message is a server response.
pub const SMB2_FLAGS_SERVER_TO_REDIR: u32 = 0x00000001;
pub const SMB2_FLAGS_ASYNC_COMMAND: u32 = 0x00000002;
pub const SMB2_FLAGS_RELATED_OPERATIONS: u32 = 0x00000004;
pub const SMB2_FLAGS_SIGNED: u32 = 0x00000008;

/// This flag is only valid for the SMB 3.1.1 dialect. It is a mask for the
/// requested I/O priority of the request, and it MUST be a value in the range
/// 0 to 7.
pub const SMB2_FLAGS_PRIORITY_MASK: u32 = 0x00000070;

pub const SMB2_FLAGS_DFS_OPERATIONS: u32 = 0x10000000;
pub const SMB2_FLAGS_REPLAY_OPERATION: u32 = 0x20000000;
