
// SMB2 commands
pub const SMB2_NEGOTIATE_PROTOCOL: u16 = 0x0000;
pub const SMB2_SESSION_SETUP: u16 = 0x0001;
pub const SMB2_LOGOFF: u16 = 0x0002;
pub const SMB2_TREE_CONNECT: u16 = 0x0003;
pub const SMB2_TREE_DISCONNECT: u16 = 0x0004;
pub const SMB2_CREATE: u16 = 0x0005;
pub const SMB2_CLOSE: u16 = 0x0006;
pub const SMB2_FLUSH: u16 = 0x0007;
pub const SMB2_READ: u16 = 0x0008;
pub const SMB2_WRITE: u16 = 0x0009;
pub const SMB2_LOCK: u16 = 0x000a;
pub const SMB2_IOCTL: u16 = 0x000b;
pub const SMB2_CANCEL: u16 = 0x000c;
pub const SMB2_ECHO: u16 = 0x000d;
pub const SMB2_QUERY_DIRECTORY: u16 = 0x000e;
pub const SMB2_CHANGE_NOTIFY: u16 = 0x000f;
pub const SMB2_QUERY_INFO: u16 = 0x0010;
pub const SMB2_SET_INFO: u16 = 0x0011;
pub const SMB2_OPLOCK_BREAK: u16 = 0x0012;
